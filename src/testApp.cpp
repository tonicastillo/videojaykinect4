#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    ofSetVerticalSync(true);
    ofBackground(0, 0, 0);
    estaFullScreen=false;
    ofSetFrameRate(24);
    ofSetCircleResolution(40);
    
//     __  ____  _  _  ___  _  _  __
//    /  \(  __)( \/ )/ __)/ )( \(  )
//   (  O )) _)  )  (( (_ \) \/ ( )(
//    \__/(__)  (_/\_)\___/\____/(__)
//    
    //GUI
    gui.setup("panel"); // most of the time you don't need a name
    //	gui.add(filled.setup("bFill", true));
//	gui.add(videoFiles.setup("videoFiles", 2, 0, 4));
	gui.add(cortoFiles.setup("cortoFiles", 3, 0, 20));
	gui.add(musicaFiles.setup( "musicaFiles", 4, 0, 20 ));
    gui.add(manchasFiles.setup( "manchasFiles", 6, 0, 20 ));
	gui.add(camaraId.setup( "camaraId", 0, 0, 10 ));
    gui.add(kinectAngle.setup( "kinectAngle", 0, -30, 30 ));
    gui.add(kinectNearThreshold.setup( "kinectNearThreshold", 255, 0, 255 ));
    gui.add(kinectFarThreshold.setup( "kinectFarThreshold", 0, 0, 255 ));
    gui.add(kinectTest.setup("kinectTest", false));
    
    gui.add(poliLineas.setup("poliLineas", 100, 10, 1000 ));
    
    gui.add(oscActivo.setup("oscActivo", false));
    
    gui.add(kinectPosX.setup("kinectPosX", 400, 0, 800));
    gui.add(kinectPosY.setup("kinectPosY", 300, 0, 600));
    gui.add(kinectAncho.setup("kinectAncho", 800, 600, 2000));
    
    gui.add(volumenFondoConcurso.setup("volumenFondoConcurso", 1.0, 0.0, 1.0));
    gui.add(volumenVideoKaraoke.setup("volumenVideoKaraoke", 1.0, 0.0, 1.0));
    gui.add(volumenVideoIntro.setup("volumenVideoIntro", 1.0, 0.0, 1.0));
    gui.add(ipIpad1.setup("ipIpad1", 1, 0, 1));
    gui.add(ipIpad2.setup("ipIpad2", 1, 0, 255));
    
    
	//gui.add(status.setup("Status", ""));
    gui.loadFromFile("settings.xml");
    bHide=false;
    
    

//      __   ____   ___    ____  ____  ____  ____  ____
//     /  \ / ___) / __)  (  _ \(  __)/ ___)(  __)(_  _)
//    (  O )\___ \( (__    )   / ) _) \___ \ ) _)   )(
//     \__/ (____/ \___)  (__\_)(____)(____/(____) (__)
//
    if(oscActivo){
        oscReceiver.setup( 12346 );
        string ipIPadTexto="192.168."+ofToString(ipIpad1.value)+"."+ofToString(ipIpad2.value);
        //    string ipIMac="192.168.0.8";
        
        oscSender.setup( ipIPadTexto , 12345 );
        
        ofxOscBundle b;
        ofxOscMessage m;
        m.setAddress( "/1/volumen/1" );
        m.addFloatArg( 1.0 );
        b.addMessage( m );
        
        m.clear();
        m.setAddress( "/1/volumen/2" );
        m.addFloatArg( 1.0 );
        b.addMessage( m );
        
        m.clear();
        m.setAddress( "/1/timelineaudio/1" );
        m.addFloatArg( 0 );
        b.addMessage( m );
        
        m.clear();
        m.setAddress( "/1/timelineaudio/2" );
        m.addFloatArg( 0 );
        b.addMessage( m );
        
        m.clear();
        m.setAddress( "/1/play/1" );
        m.addIntArg(0);
        b.addMessage( m );
        
        m.clear();
        m.setAddress( "/1/play/2" );
        m.addIntArg(0);
        b.addMessage( m );
        
        ofxOscBundle nested_b;
        for(int i1=1; i1<=2; i1++){
            for(int i2=1; i2<=3; i2++){
                for (int i3=1; i3<=5; i3++){
                    m.clear();
                    m.setAddress( "/1/audio/"+ofToString(i1)+"/"+ofToString(i2)+"/"+ofToString(i3) );
                    m.addIntArg(0);
                    //                std::cout << "/1/audio/"+ofToString(i1)+"/"+ofToString(i2)+"/"+ofToString(i3)+"\n";
                    nested_b.addMessage( m );
                    //                /1/audio/1/1/5
                }
            }
        }
        b.addBundle(nested_b);
        
        ofxOscBundle nested_b2;
        for (int i1=1; i1<=3; i1++){
            m.clear();
            m.setAddress( "/1/alphavideo/"+ofToString(i1) );
            m.addFloatArg(1.0);
            nested_b2.addMessage( m );
            
            m.clear();
            m.setAddress( "/1/timelinevideo/"+ofToString(i1) );
            m.addFloatArg(0.0);
            nested_b2.addMessage( m );
            
            m.clear();
            m.setAddress( "/1/activovideo/"+ofToString(i1) );
            m.addIntArg(0);
            nested_b2.addMessage( m );
            
            m.clear();
            m.setAddress( "/1/pausadovideo/"+ofToString(i1) );
            m.addIntArg(0);
            nested_b2.addMessage( m );
        }
        
        m.clear();
        m.setAddress( "/1/webcam" );
        m.addIntArg(0);
        b.addMessage( m );
        m.clear();
        m.setAddress( "/1/policia" );
        m.addIntArg(0);
        b.addMessage( m );
        m.clear();
        m.setAddress( "/1/hiphop" );
        m.addIntArg(0);
        m.clear();
        m.setAddress( "/2/dibujaluces" );
        m.addFloatArg(1);
        
        b.addMessage( m );
        
        
        b.addBundle(nested_b2);
        
        oscSender.sendBundle( b );
        current_msg_string = 0;
    }
    

 
//     _  _  __  ____  ____  __     _  _     __   _  _  ____  __  __      ___   __   ____   ___   __
//    / )( \(  )(    \(  __)/  \   ( \/ )   / _\ / )( \(    \(  )/  \    / __) / _\ (  _ \ / __) / _\
//    \ \/ / )(  ) D ( ) _)(  O )   )  /   /    \) \/ ( ) D ( )((  O )  ( (__ /    \ )   /( (_ \/    \
//     \__/ (__)(____/(____)\__/   (__/    \_/\_/\____/(____/(__)\__/    \___)\_/\_/(__\_) \___/\_/\_/
//
    //VIDEOS Y 
    //for(int i=1;i<=videoFiles;i++){
        ofVideoPlayer myPlayer;
        videos[0]=myPlayer;
        videos[0].loadMovie("videoslargos/"+ofToString(1)+".mp4");
        videos[0].setVolume(REDUCCIONAUDIO);
        std::cout << "Cargando " << ("videoslargos/"+ofToString(1)+".mp4") << "\n";
        videos[0].stop();
        videos[0].setLoopState(OF_LOOP_NONE);
        bVideoLargo1=false;

    ofVideoPlayer myPlayer2;
    videos[1]=myPlayer2;
    videos[1].loadMovie("videoslargos/"+ofToString(2)+".mp4");
    videos[1].setVolume(REDUCCIONAUDIO);
    std::cout << "Cargando " << ("videoslargos/"+ofToString(2)+".mp4") << "\n";
    videos[1].stop();
    videos[1].setLoopState(OF_LOOP_NONE);
    bVideoLargo1=false;

    
    //}
    
    //VIDEOS CORTOS
    for(int i=1;i<=cortoFiles;i++){
        ofVideoPlayer myPlayer;
        
        
        cortos[i-1]=myPlayer;
        cortos[i-1].loadMovie("videoscortos/"+ofToString(i)+".mp4");
        cortos[i-1].setVolume(REDUCCIONAUDIO);
        std::cout << "Cargando " << ("videoscortos/"+ofToString(i)+".mp4") << "\n";
        cortos[i-1].stop();
        cortosactivo[i-1]=false;
        cortos[i-1].setLoopState(OF_LOOP_NONE);
    }
    // AUDIO
    for(int i=1;i<=musicaFiles;i++){
        ofSoundPlayer mySoundPlayer;
        sonidos[0][i-1]=mySoundPlayer;
        sonidos[0][i-1].loadSound("musicas/"+ofToString(i)+".mp3");
        sonidosactivo[0][i-1]=false;
        ofSoundPlayer mySoundPlayer2;
        sonidos[1][i-1]=mySoundPlayer2;
        sonidos[1][i-1].loadSound("musicas/"+ofToString(i)+".mp3");
        sonidos[1][i-1].setVolume(REDUCCIONAUDIO);
        sonidosactivo[1][i-1]=false;
        std::cout << "Cargando " << ("musicas/"+ofToString(i)+".mp3") << "\n";
        
    }
    sonidosultimo[0]=-1;
    sonidosultimo[1]=-1;
    
    //FOTOS CONCURSO
    for(int i=0;i<12;i++){
        concursoImagenes[i].loadImage("millonario/"+ofToString(i)+".png");
        concursoImagenesActivas[i]=false;
    }
    concursoSonido.loadSound("millonario/fondo2.mp3");
    concursoSonido.setLoop(true);
//    concursoVideo1.loadMovie("videoscortos/"+ofToString(i)+".mp4");
//    cortos[i-1].setVolume(REDUCCIONAUDIO);
//    std::cout << "Cargando " << ("videoscortos/"+ofToString(i)+".mp4") << "\n";
//    cortos[i-1].stop();
//    cortosactivo[i-1]=false;
//    cortos[i-1].setLoopState(OF_LOOP_NONE);
//    concursoVideo2;
    
//     _  _  ____  ____   ___   __   _  _
//    / )( \(  __)(  _ \ / __) / _\ ( \/ )
//    \ /\ / ) _)  ) _ (( (__ /    \/ \/ \
//    (_/\_)(____)(____/ \___)\_/\_/\_)(_/
//    
    //CAMARA
//	camara.setVerbose(true);
    //camara.setDeviceID(camaraId);
    std::cout << "camara width " << ofToString(camara.getWidth()) << "\n";
    std::cout << "camara height " << ofToString(camara.getHeight()) << "\n";
    
//	camara.initGrabber(640,480);


    
//     __ _  __  __ _  ____  ___  ____    ____  ____  ____  _  _  ____
//    (  / )(  )(  ( \(  __)/ __)(_  _)  / ___)(  __)(_  _)/ )( \(  _ \
//     )  (  )( /    / ) _)( (__   )(    \___ \ ) _)   )(  ) \/ ( ) __/
//    (__\_)(__)\_)__)(____)\___) (__)   (____/(____) (__) \____/(__)
//    
    
    //KINECT
        kinect.setRegistration(true);
    //
    	kinect.init(false);
    //	//kinect.init(true); // shows infrared instead of RGB video image
    //	//kinect.init(false, false); // disable video image (faster fps)
        kinect.open();
    //    kinectGrayImage.allocate(kinect.width, kinect.height);
    kinect.setCameraTiltAngle(kinectAngle);
    kinectImagenActual.allocate(kinect.getWidth(), kinect.getHeight(), OF_IMAGE_GRAYSCALE);
    kinectColorImg.allocate(kinect.width, kinect.height);

    
//     ____   __   __    __    __  _  _   __    ___  ____  __ _  ____  ____    ____   __   __ _   ___  ____  ____
//    (  _ \ /  \ (  )  (  )  (  )( \/ ) / _\  / __)(  __)(  ( \(  __)/ ___)  / ___) / _\ (  ( \ / __)(  _ \(  __)
//     ) __/(  O )/ (_/\ )(    )( / \/ \/    \( (_ \ ) _) /    / ) _) \___ \  \___ \/    \/    /( (_ \ )   / ) _)
//    (__)   \__/ \____/(__)  (__)\_)(_/\_/\_/ \___/(____)\_)__)(____)(____/  (____/\_/\_/\_)__) \___/(__\_)(____)
//    
    //POLI
//    srcImg.loadImage("A.jpg");
//    puntoBlanco.loadImage("puntoblanco.png");
//    
    for(int i=0; i<manchasFiles; i++){
        manchasImagen[i].loadImage("manchas/"+ofToString(i)+".png");
    }
    
    sangreActiva=false;
    poliLuces=1;
    
    
    ofEnableAlphaBlending();
    fboPoli.allocate(ofGetWindowWidth(), ofGetWindowHeight());
    fboPoliContenido.allocate(ofGetWindowWidth(), ofGetWindowHeight());
    fboPoliMask.allocate(ofGetWindowWidth(), ofGetWindowHeight());
    
    // para limpiar, para que no haya errores
    fboPoli.begin();
    ofClear(0,0,0,255);
    fboPoli.end();
    fboPoliMask.begin();
    ofClear(0,0,0,255);
    fboPoliMask.end();
    
    imagePoliContenido.allocate(ofGetWindowWidth(), ofGetWindowHeight(), OF_IMAGE_COLOR_ALPHA);
    
    string shaderProgram = "#version 120\n \
    #extension GL_ARB_texture_rectangle : enable\n \
    \
    uniform sampler2DRect tex0;\
    uniform sampler2DRect maskTex;\
    \
    void main (void){\
    vec2 pos = gl_TexCoord[0].st;\
    \
    vec3 src = texture2DRect(tex0, pos).rgb;\
    float mask = texture2DRect(maskTex, pos).r;\
    \
    gl_FragColor = vec4( src , mask);\
    }";
    shaderPoli.setupShaderFromSource(GL_FRAGMENT_SHADER, shaderProgram);
    shaderPoli.linkProgram();
    
    //Lista de webcams
    //camara.listDevices();
    
    //     __ _  ____  ____  _  _   __  ____  __ _
    //    (  ( \(  __)(_  _)/ )( \ /  \(  _ \(  / )
    //    /    / ) _)   )(  \ /\ /(  O ))   / )  (
    //    \_)__)(____) (__) (_/\_) \__/(__\_)(__\_)
    //
    
    udpConnection.Create();
    udpConnection.Connect("127.0.0.1",11998);
    udpConnection.Bind(11999);
    udpConnection.SetNonBlocking(true);

}

















//                _  _  ____  ____   __  ____  ____
// ___  ___  ___ / )( \(  _ \(    \ / _\(_  _)(  __)___  ___  ___
//(___)(___)(___)) \/ ( ) __/ ) D (/    \ )(   ) _)(___)(___)(___)
//               \____/(__)  (____/\_/\_/(__) (____)
//

//--------------------------------------------------------------
void testApp::update(){
    bool bKinectUpdateada=false;
        if(bVideoLargo1){
            videos[0].update();
            videos[0].setVolume(volumenVideoIntro);
        }
    if(bVideoLargo2){
        videos[1].update();
        videos[1].setVolume(volumenVideoKaraoke);
    }

    
    
    for(int i=1;i<=cortoFiles;i++){
        if(cortosactivo[i-1]){
            //            if(cortos[i-1].getPosition()>0){
            cortos[i-1].update();
            //            }
            cout << "cortos[" << ofToString(i-1) << "].getPosition() " << ofToString(cortos[i-1].getPosition()) << endl;
            //if(!cortos[i-1].isPlaying()){
            if(cortos[i-1].getPosition()>=1){
                cout << "fin corto " << ofToString(i-1) << endl;
                cortosactivo[i-1]=false;
                cortos[i-1].stop();
                cortos[i-1].setPosition(0);
            }
        }
    }
    if(oscActivo){
//     __   ____   ___    ____  ____  ___  __  ____  _  _  ____  ____
//    /  \ / ___) / __)  (  _ \(  __)/ __)(  )(  __)/ )( \(  __)(  _ \
//   (  O )\___ \( (__    )   / ) _)( (__  )(  ) _) \ \/ / ) _)  )   /
//    \__/ (____/ \___)  (__\_)(____)\___)(__)(____) \__/ (____)(__\_)
//
        //OSC RECIEVER
        for ( int i=0; i<NUM_MSG_STRINGS; i++ )
        {
            if ( timers[i] < ofGetElapsedTimef() )
                msg_strings[i] = "";
        }
        // check for waiting messages
        bool hiphopHazFotoAhora=false;
        while( oscReceiver.hasWaitingMessages() )
        {
            // get the next message
            ofxOscMessage m;
            oscReceiver.getNextMessage( &m );
            
            
            std::cout << "mensaje" << m.getAddress() << "\n";
            
            //         __   ____   ___    _  _  __  ____  ____  __   ____
            //        /  \ / ___) / __)  / )( \(  )(    \(  __)/  \ / ___)
            //       (  O )\___ \( (__   \ \/ / )(  ) D ( ) _)(  O )\___ \
            //        \__/ (____/ \___)   \__/ (__)(____/(____)\__/ (____/
            //        
            // VIDEOS LARGOS
            if(m.getAddress().substr(0,15)=="/1/activovideo/"){
                int nvideo = ofToInt( m.getAddress().substr(15,1) )-1;
                std::cout << "nvideo" << nvideo << "\n";
                
                if(nvideo==0){
                    bVideoLargo1=m.getArgAsFloat(0);
                    if(bVideoLargo1){
                        videos[nvideo].play();
                    } else {
                        videos[nvideo].stop();
                    }
                } else if(nvideo==1){
                    bVideoLargo2=m.getArgAsFloat(0);
                    if(bVideoLargo2){
                        videos[nvideo].play();
                    } else {
                        videos[nvideo].stop();
                    }
                }
                
            }
            // VIDEO PAUSADO
            if(m.getAddress().substr(0,16)=="/1/pausadovideo/"){
                int nvideo = ofToInt( m.getAddress().substr(16,1) )-1;
                std::cout << "nvideo" << nvideo << "\n";
                if(nvideo<videoFiles){
                    if(m.getArgAsFloat(0)){
                        videos[nvideo].setPaused(true);
                    } else {
                        videos[nvideo].setPaused(false);
                    }
                    
                }
                
            }
            
            //        // VIDEO ALPHA
            //        if(m.getAddress().substr(0,14)=="/1/alphavideo/"){
            //            int nvideo = ofToInt( m.getAddress().substr(14,1) )-1;
            //            std::cout << "nvideo" << nvideo << "\n";
            //            if(nvideo<videoFiles){
            //                videosAlpha[nvideo]=int(255*m.getArgAsFloat(0));
            //            }
            //            
            //        }
            
            // VIDEO POSICION
            if(m.getAddress().substr(0,17)=="/1/timelinevideo/"){
                int nvideo = ofToInt( m.getAddress().substr(17,1) )-1;
                float valor = m.getArgAsFloat(0);
                std::cout << "posicionando videosnvideo " << nvideo << "\n";
                std::cout << "valor " << valor  << "\n";
                //if(nvideo<videoFiles){
                videos[nvideo].setPosition(valor);
                //}
                
                
            }
            // VIDEOS CORTOS
            if(m.getAddress().substr(0,16)=="/Video_corto_on/"){
                int nvideo = ofToInt( m.getAddress().substr(16,1) )-1;
                cortosactivo[nvideo]=true;
                cortos[nvideo].play();
                
            }
            if(m.getAddress().substr(0,17)=="/Video_corto_off/"){
                int nvideo = ofToInt( m.getAddress().substr(17,1) )-1;
                cortosactivo[nvideo]=false;
                cortos[nvideo].stop();
                cortos[nvideo].setPosition(0);
            }
            
            //          __   ____   ___     __   _  _  ____  __  __   ____
            //         /  \ / ___) / __)   / _\ / )( \(    \(  )/  \ / ___)
            //        (  O )\___ \( (__   /    \) \/ ( ) D ( )((  O )\___ \
            //         \__/ (____/ \___)  \_/\_/\____/(____/(__)\__/ (____/
            //        
            // AUDIO
            if(m.getAddress().substr(0,9)=="/1/audio/"){
                int canal = ofToInt( m.getAddress().substr(9,1) )-1;
                int nsonido = ( 3 - ofToInt(  m.getAddress().substr(11,1) ) )*5 + ofToInt( m.getAddress().substr(13,1) ) -1 ;
                
                //Desactiva los otros botones en el osc
                ofxOscBundle b;
                ofxOscMessage m2;
                for(int i2=1; i2<=3; i2++){
                    for (int i3=1; i3<=5; i3++){
                        if(i2 != ofToInt(  m.getAddress().substr(11,1) ) || i3 != ofToInt( m.getAddress().substr(13,1) )){
                            m2.setAddress( "/1/audio/"+ofToString(canal+1)+"/"+ofToString(i2)+"/"+ofToString(i3) );
                            m2.addIntArg(0);
                            //                std::cout << "/1/audio/"+ofToString(i1)+"/"+ofToString(i2)+"/"+ofToString(i3)+"\n";
                            b.addMessage( m2 );
                            //                /1/audio/1/1/5
                            m2.clear();
                        }
                    }
                }
                b.addMessage(m2);
                oscSender.sendBundle(b);
                
                
                std::cout << "nsonido " << nsonido << "\n";
                std::cout << "valor " << m.getArgAsFloat(0) << "\n";
                
                if (nsonido<musicaFiles) {
                    sonidosactivo[canal][nsonido]=m.getArgAsFloat(0);
                    
                    
                    if(sonidosactivo[canal][nsonido]){
                        //sonidos[canal][nsonido].play();
                        sonidosultimo[canal]=nsonido;
                    } else {
                        //sonidos[canal][nsonido].stop();
                        sonidosultimo[canal]=-1;
                    }
                    
                }
            }
            //VOLUMEN
            if(m.getAddress().substr(0,11)=="/1/volumen/"){
                int canal = ofToInt( m.getAddress().substr(11,1) )-1;
                float valor = m.getArgAsFloat(0);
                
                std::cout << "volumen canal " << canal << "\n";
                std::cout << "valor " << valor  << "\n";
                for(int i=1;i<=musicaFiles;i++){
                    sonidos[canal][i-1].setVolume(valor*REDUCCIONAUDIO);
                }
            }
            //PLAY
            if(m.getAddress().substr(0,8)=="/1/play/"){
                int canal = ofToInt( m.getAddress().substr(8,1) )-1;
                float valor = m.getArgAsInt32(0);
                
                std::cout << "play canal " << canal << "\n";
                std::cout << "valor " << valor  << "\n";
                if(valor==1){
                    std::cout << "valor " << sonidosultimo[canal]  << "\n";
                    if(sonidosultimo[canal]>=0){
                        sonidos[canal][sonidosultimo[canal]].play();
                        std::cout << "Reproduce \n";
                    }
                    
                    
                } else {
                    for(int i1=0;i1<NSONIDOS;i1++){
                        if(sonidos[canal][i1].getIsPlaying()){
                            sonidos[canal][i1].stop();
                        }
                    }
                }
            }
            //POSICION AUDIO
            if(m.getAddress().substr(0,17)=="/1/timelineaudio/"){
                int canal = ofToInt( m.getAddress().substr(17,1) )-1;
                float valor = m.getArgAsFloat(0);
                
                std::cout << "posicion canal " << canal << "\n";
                std::cout << "valor " << valor  << "\n";
                if(sonidosultimo[canal]>=0){
                    sonidos[canal][sonidosultimo[canal]].setPosition(valor);
                }
                
                
            }
            //BOTON WEBCAM
            if(m.getAddress().substr(0,9)=="/3/webcam"){
                int valor = m.getArgAsInt32(0);
                parteWebcam=valor;
                std::cout << "Webcam \n";
                std::cout << "valor " << valor  << "\n";
                if(!valor){
                    //Desactiva los otros botones en el osc
                    ofxOscBundle b;
                    ofxOscMessage m;
                    for(int i=0; i<12; i++){
                        m.setAddress( "/3/concurso/"+ofToString(i) );
                        m.addIntArg(0);
                        //                std::cout << "/1/audio/"+ofToString(i1)+"/"+ofToString(i2)+"/"+ofToString(i3)+"\n";
                        b.addMessage( m );
                        //                /1/audio/1/1/5
                        m.clear();
                    }
                    b.addMessage(m);
                    oscSender.sendBundle(b);
                }
                
            }
            //Concurso imagenes
            if(m.getAddress().substr(0,12)=="/3/concurso/"){
                int imagen = ofToInt( m.getAddress().substr(12,2) );
                int valor = m.getArgAsFloat(0);
                std::cout << "concurso \n";
                std::cout << "imagen " << imagen  << "\n";
                if(imagen==6){
                    concursoImagenesActivas[0]=false;
                    concursoImagenesActivas[1]=false;
                    concursoImagenesActivas[2]=false;
                    concursoImagenesActivas[3]=false;
                    concursoImagenesActivas[4]=false;
                    concursoImagenesActivas[5]=false;
                }
                concursoImagenesActivas[imagen]=valor;
            }
            
            // concurso imagenes quita
            if(m.getAddress().substr(0,26)=="/3/concurso_quitaimagenes/"){
                //int imagen = ofToInt( m.getAddress().substr(12,2) );
                //int valor = m.getArgAsFloat(0);
                //std::cout << "concurso \n";
                std::cout << "quita imagenes\n";
                for(int i=0;i<12;i++){
                    concursoImagenesActivas[i]=false;
                }
                ofxOscBundle b;
                ofxOscMessage m;
                for(int i=0; i<12; i++){
                    m.setAddress( "/3/concurso/"+ofToString(i) );
                    m.addIntArg(0);
                    //                std::cout << "/1/audio/"+ofToString(i1)+"/"+ofToString(i2)+"/"+ofToString(i3)+"\n";
                    b.addMessage( m );
                    //                /1/audio/1/1/5
                    m.clear();
                }
                b.addMessage(m);
                oscSender.sendBundle(b);
            }
            
            //Concurso Sonido
            if(m.getAddress().substr(0,18)=="/3/sonidoconcurso/"){
                int valor = m.getArgAsFloat(0);
                if(valor){
                    concursoSonido.setVolume(volumenFondoConcurso);
                    concursoSonido.play();
                } else {
                    concursoSonido.stop();
                }
            }
            
            //BOTON POLI
            if(m.getAddress().substr(0,10)=="/1/policia"){
                int valor = m.getArgAsInt32(0);
                partePoli=valor;
                std::cout << "partePoli\n";
                std::cout << "valor " << valor  << "\n";
                
            }
            //BOTON HIPHOP
            if(m.getAddress().substr(0,9)=="/1/hiphop"){
                int valor = m.getArgAsInt32(0);
                parteHip=valor;
                std::cout << "parteHip \n";
                std::cout << "valor " << valor  << "\n";
                
            }
            //HIPHOP FOTO
            if(m.getAddress().substr(0,13)=="/1/fotohiphop"){
                hiphopHazFotoAhora=true;
                std::cout << "HipHopFoto \n";
                
                
            }        
            
            //MANCHAS A�ADE
            if(m.getAddress().substr(0,9)=="/2/sangre"){
                float valorX = m.getArgAsFloat(1);
                float valorY = m.getArgAsFloat(0);
                int imagenActual=int(ofRandom(manchasFiles));
                std::cout << "SANGRE \n";
                std::cout << "valorX " << valorX  << "\n";
                std::cout << "valorY " << valorY  << "\n";
                if(sangreActiva){
                    manchasSoltadas.push_back(ofPoint(valorX,valorY,imagenActual));
                } else {
                    Circulos circuloActual=Circulos(valorX*ofGetWindowWidth(), valorY*ofGetWindowHeight());
                    listaCirculos.push_back(circuloActual);
                }
            }
            //MANCHAS ACTIVO
            if(m.getAddress().substr(0,15)=="/2/dibujasangre"){
                float activo = m.getArgAsFloat(0);
                sangreActiva=activo;
                std::cout << "Dibuja SANGRE " << ofToString(sangreActiva) << " \n";
                
            }
            //MANCHAS BORRA TODAS
            if(m.getAddress().substr(0,14)=="/2/borrasangre"){
                manchasSoltadas.clear();
            }
            //LUCES ACTIVO
            if(m.getAddress().substr(0,14)=="/2/dibujaluces"){
                float valor = m.getArgAsFloat(0);
                poliLuces=valor;
                std::cout << "Dibuja LUCES " << ofToString(poliLuces) << " \n";
            }
            
        }
        
        
        //     __   ____   ___    ____  _  _  __  ____  ____  ____  ____
        //    /  \ / ___) / __)  (  __)( \/ )(  )(_  _)(_  _)(  __)(  _ \
        //   (  O )\___ \( (__    ) _) / \/ \ )(   )(    )(   ) _)  )   /
        //    \__/ (____/ \___)  (____)\_)(_/(__) (__)  (__) (____)(__\_)
        //
        
        //OSC EMITTER
        // Punto del audio
        ofxOscBundle b;
        ofxOscMessage m;
        if(ofGetFrameNum()%10==0){
            for(int i1=0;i1<=1;i1++){
                if(sonidosultimo[i1]>=0){
                    float posicion = sonidos[i1][sonidosultimo[i1]].getPosition();
                    if(sonidos[i1][sonidosultimo[i1]].getIsPlaying()){
                        std::cout << "Esta reproduciendo " << sonidos[i1][sonidosultimo[i1]].getIsPlaying() << "\n";
                        sonidosposicion[i1]=posicion;
                    } else {
                        posicion=sonidosposicion[i1];
                    }
                    if(posicion>=0.0){
                        m.setAddress( "/1/timelineaudio/"+ofToString(i1+1) );
                        m.addFloatArg( posicion );
                        b.addMessage(m);
                        m.clear();
                    }
                    
                }
                
                
            }
            
            if(videos[0].isLoaded()){
                float posicion = videos[0].getPosition();
                if(bVideoLargo1){
                    fVideoLargoPosicion1=posicion;
                } else {
                    posicion=fVideoLargoPosicion1;
                }
                if(posicion>=0.0){
                    m.setAddress( "/1/timelinevideo/"+ofToString(1) );
                    m.addFloatArg( posicion );
                    b.addMessage(m);
                    m.clear();
                }
            }
            if(videos[1].isLoaded()){
                float posicion = videos[1].getPosition();
                if(bVideoLargo2){
                    fVideoLargoPosicion2=posicion;
                } else {
                    posicion=fVideoLargoPosicion2;
                }
                if(posicion>=0.0){
                    m.setAddress( "/1/timelinevideo/"+ofToString(2) );
                    m.addFloatArg( posicion );
                    b.addMessage(m);
                    m.clear();
                }
            }
            
            
        }
        oscSender.sendBundle(b);
        
    }
    
    
    //CAMARA
    if(parteWebcam){
        //camara.update();
//        std::cout << "actualizando webcam \n";
        kinect.update();
        bKinectUpdateada=true;
    }
    
//     __ _  __  __ _  ____  ___  ____
//    (  / )(  )(  ( \(  __)/ __)(_  _)
//     )  (  )( /    / ) _)( (__   )(
//    (__\_)(__)\_)__)(____)\___) (__)
//    
    // KINECT
    if(partePoli || parteHip){
        if(!bKinectUpdateada){
            kinect.update();
        }
        
        
        // there is a new frame and we are connected
        if(kinect.isFrameNew()) {
            
            // load grayscale depth image from the kinect source
            kinectGrayImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);
            
            
            // we do two thresholds - one for the far plane and one for the near plane
            // we then do a cvAnd to get the pixels which are a union of the two thresholds
            //if(bThreshWithOpenCV) {
			kinectGrayThreshNear = kinectGrayImage;
			kinectGrayThreshFar = kinectGrayImage;
			kinectGrayThreshNear.threshold(kinectNearThreshold, true);
			kinectGrayThreshFar.threshold(kinectFarThreshold);
			cvAnd(kinectGrayThreshNear.getCvImage(), kinectGrayThreshFar.getCvImage(), kinectGrayImage.getCvImage(), NULL);
            
            
            // update the cv images
            kinectGrayImage.flagImageChanged();
            kinectColorImg.flagImageChanged();
            
            //kinectImagenActual = kinectGrayImage.getPixelsRef();
            //    kinectGrayImage.getTextureReference();
//            if(hiphopHazFotoAhora){
//                hiphopFotos[0].setFromPixels(kinect.getPixels(), kinect.width, kinect.height, OF_IMAGE_COLOR);
//            }
//            kinectImagenActualColor.setFromPixels(kinect.getPixels(), kinect.width, kinect.height, OF_IMAGE_COLOR);
            
            // find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
            // also, find holes is set to true so we will get interior contours as well....
            //kinectContourFinder.findContours(kinectGrayImage, 10, (kinect.width*kinect.height)/2, 20, false);
            
            
            
        }
        
        
        
    } else if(kinectTest){
        if(!bKinectUpdateada){
            kinect.update();
        }
    }
   


    //     __ _  ____  ____  _  _   __  ____  __ _
    //    (  ( \(  __)(_  _)/ )( \ /  \(  _ \(  / )
    //    /    / ) _)   )(  \ /\ /(  O ))   / )  (
    //    \_)__)(____) (__) (_/\_) \__/(__\_)(__\_)
    //
    
    //RECIBIR
    char udpMessage[100000];
    udpConnection.Receive(udpMessage,100000);
    string message=udpMessage;
    if(message!=""){
        vector<string> variables = ofSplitString(message,"[/p]");
        for(int i=0;i<variables.size();i++){
            vector<string> partes=ofSplitString(variables[i],"|");
            if(partes.size()==2){
                std::cout << "Mensaje " << partes[0] << " = " << partes[1] << "\n";
                if(partes[0]=="bVideoLargo1"){
                    bVideoLargo1=ofToBool(partes[1]);
                    if(bVideoLargo1){
                        videos[0].play();
                        std::cout << "PPLLLAAYYYY\n";

                    } else {
                        videos[0].stop();
                    }
                }
                if(partes[0]=="bVideoLargo2"){
                    bVideoLargo2=ofToBool(partes[1]);
                    if(bVideoLargo2){
                        videos[1].play();
                    } else {
                        videos[1].stop();
                    }
                }
                if(partes[0]=="fVideoLargoPosicion1"){
                    videos[0].setPosition(ofToFloat(partes[1]));
                }
                if(partes[0]=="fVideoLargoPosicion2"){
                    videos[1].setPosition(ofToFloat(partes[1]));
                }
                if(partes[0]=="bConcursoMillonarioActivo"){
                    bool valor=ofToBool(partes[1]);
                    if(valor){
                        concursoSonido.setVolume(volumenFondoConcurso);
                        concursoSonido.play();
                        parteWebcam=true;
                        cortosactivo[1]=true;
                        cortos[1].play();
                    } else {
                        concursoSonido.stop();
                        parteWebcam=false;
                    }
                }
                if(partes[0]=="bConcursoMillonarioVideoAcertada"){
                    bool valor=ofToBool(partes[1]);
                    if(valor){
                        cortosactivo[2]=true;
                        cortos[2].play();
                        for(int aux=0; aux<=11;aux++){
                            concursoImagenesActivas[aux]=false;
                        }
                    } else {
                        cortosactivo[2]=false;
                        cortos[2].stop();
                        cortos[2].setPosition(0);
                    }
                }
//                if(partes[0]=="bConcursoMillonarioVideoFallo"){
//                    cortosactivo[3]=true;
//                    cortos[3].play();
//                }
                if(partes[0]=="iConcursoMillonarioImagenPregunta1"){
                    int preguntan= ofToFloat(partes[1]);
                    for(int aux=0; aux<12;aux++){
                        concursoImagenesActivas[aux]=false;
                    }
                    concursoImagenesActivas[preguntan]=true;
                    
                }
                if(partes[0]=="iConcursoMillonarioImagenPregunta1"){
                    int preguntan= ofToFloat(partes[1])-1;
                    for(int aux=0; aux<=11;aux++){
                        concursoImagenesActivas[aux]=false;
                    }
                    for(int aux=0; aux<=preguntan;aux++){
                        concursoImagenesActivas[aux]=true;
                    }
                }
                if(partes[0]=="iConcursoMillonarioImagenPregunta2"){
                    int preguntan = ofToFloat(partes[1])-1;
                    for(int aux=0; aux<12;aux++){
                        concursoImagenesActivas[aux]=false;
                    }
                    if(preguntan>=0){
                        for(int aux=6; aux<=preguntan+6;aux++){
                            concursoImagenesActivas[aux]=true;
                        }
                    }
                }
                if(partes[0]=="bConcursoUnDosTresActivo"){
                    bool valor=ofToBool(partes[1]);
                    if(valor){
                        cortosactivo[0]=true;
                        cortos[0].play();
                    } else {
                        cortosactivo[0]=false;
                        cortos[0].stop();
                        cortos[0].setPosition(0);
                    }
                }
                
                if(partes[0]=="bPoliActivo"){
                    bool valor=ofToBool(partes[1]);
                    partePoli=valor;
                }
                if(partes[0]=="fPoliRayosAlfa"){
                    float valor=ofToFloat(partes[1]);
                    poliLuces=valor;
                }
                if(partes[0]=="pPoliCirculo"){
                    std::cout << "pPoliCirculo partes[1] " << ofToString(partes[1]) << "\n";
                    std::cout << "pPoliCirculo partes[1][0] " << ofToString(ofSplitString(partes[1], ",")[0]) << "\n";
                    std::cout << "pPoliCirculo partes[1][1] " << ofToString(ofSplitString(partes[1], ",")[1]) << "\n";
                    
                    float valorX=ofToFloat(ofSplitString(partes[1], ",")[0]);
                    float valorY=ofToFloat(ofSplitString(partes[1], ",")[1]);
                    sangreActiva=false;
                    Circulos circuloActual=Circulos(valorX*ofGetWindowWidth(), valorY*ofGetWindowHeight());
                    listaCirculos.push_back(circuloActual);
                }
                if(partes[0]=="bPoliBorraSangre"){
                    manchasSoltadas.clear();
                }
                
                if(partes[0]=="pPoliSangre"){
                    float valorX=ofToFloat(ofSplitString(partes[1], ",")[0]);
                    float valorY=ofToFloat(ofSplitString(partes[1], ",")[1]);
                    sangreActiva=true;
                    int imagenActual=int(ofRandom(manchasFiles));
                    manchasSoltadas.push_back(ofPoint(valorX,valorY,imagenActual));
                }
                
                
//                //MANCHAS A�ADE
//                if(m.getAddress().substr(0,9)=="/2/sangre"){
//                    float valorX = m.getArgAsFloat(1);
//                    float valorY = m.getArgAsFloat(0);
//                    int imagenActual=int(ofRandom(manchasFiles));
//                    std::cout << "SANGRE \n";
//                    std::cout << "valorX " << valorX  << "\n";
//                    std::cout << "valorY " << valorY  << "\n";
//                    if(sangreActiva){
//                        manchasSoltadas.push_back(ofPoint(valorX,valorY,imagenActual));
//                    } else {
//                        Circulos circuloActual=Circulos(valorX*ofGetWindowWidth(), valorY*ofGetWindowHeight());
//                        listaCirculos.push_back(circuloActual);
//                    }
//                }
//                //MANCHAS ACTIVO
//                if(m.getAddress().substr(0,15)=="/2/dibujasangre"){
//                    float activo = m.getArgAsFloat(0);
//                    sangreActiva=activo;
//                    std::cout << "Dibuja SANGRE " << ofToString(sangreActiva) << " \n";
//                    
//                }
//                //MANCHAS BORRA TODAS
//                if(m.getAddress().substr(0,14)=="/2/borrasangre"){
//                    manchasSoltadas.clear();
//                }

            }
            
        }
    }
    
    if(bVideoLargo1){
        std::cout << "VIDEO LARGO 1 ON\n";
    }
    if(bVideoLargo2){
        std::cout << "VIDEO LARGO 2 ON\n";
    }
}







//                  ____  ____   __   _  _                 
//    ___  ___  ___(    \(  _ \ / _\ / )( \ ___  ___  ___  
//   (___)(___)(___)) D ( )   //    \\ /\ /(___)(___)(___) 
//                 (____/(__\_)\_/\_/(_/\_)                

//--------------------------------------------------------------
void testApp::draw(){
//    ofClear(0, 0, 0);
//    
    ofBackground(0, 0, 0);
    ofSetColor(255,255,255);
    //CAMARA
    
    if(parteWebcam){
        kinect.draw(0, 0, ofGetWindowWidth(), ofGetWindowHeight());
        //CONCURSO
        for(int i=0;i<12;i++){
            if(concursoImagenesActivas[i]){
                concursoImagenes[i].draw(0,0,ofGetWindowWidth(), ofGetWindowHeight());
            }
        }
    }


    
    
    
    //for(int i=1;i<=videoFiles;i++){
        ofVideoPlayer video;
        if(bVideoLargo1){
            video=videos[0];
            ofDisableAlphaBlending();
            video.draw(0,0,ofGetWindowWidth(),ofGetWindowHeight());
            ofEnableAlphaBlending();
        }
    if(bVideoLargo2){
        video=videos[1];
        ofDisableAlphaBlending();
        video.draw(0,0,ofGetWindowWidth(),ofGetWindowHeight());
        ofEnableAlphaBlending();
    }
    //}
    for(int i=1;i<=cortoFiles;i++){
        if(cortosactivo[i-1]){
//            if(cortos[i-1].getPosition()>0){
                cortos[i-1].draw(0,0,ofGetWindowWidth(),ofGetWindowHeight());
//            }
        }
    }
    if(kinectTest){
        kinect.draw(320, 0, 320, 240);
    }
    
    if(partePoli || parteHip){
        
        
        
        fboPoliMask.begin();
        //float kAlto=kinectAncho*0.75;
        //kinectGrayImage.draw(kinectPosX-kinectAncho*0.5, kinectPosY-kAlto*0.5, kinectAncho, kAlto);
        float kAlto=kinectAncho*0.75;
        //cout << "nose 1" << endl;
        kinectGrayImage.draw(kinectPosX-kinectAncho*0.5, kinectPosY-kAlto*0.5, kinectAncho, kAlto);
        //cout << "nose 2" << endl;
        fboPoliMask.end();
        
        
        
        if(partePoli){
            
            //     ____   __   ____  ____  ____    ____   __   __    __    ____  ____   __   _  _
            //    (  _ \ / _\ (  _ \(_  _)(  __)  (  _ \ /  \ (  )  (  )  (    \(  _ \ / _\ / )( \
            //     ) __//    \ )   /  )(   ) _)    ) __/(  O )/ (_/\ )(    ) D ( )   //    \\ /\ /
            //    (__)  \_/\_/(__\_) (__) (____)  (__)   \__/ \____/(__)  (____/(__\_)\_/\_/(_/\_)
            //
            
            
//          Sangre
            fboPoliContenido.begin();
            ofClear(0, 0, 0, 0);
            
            
//          luces
            if(poliLuces>0.1){
                ofSetLineWidth(2);
                for(int i=0;i<=poliLineas;i++){
                    ofSetColor(0, 255, 0,255*poliLuces);
                    ofLine(ofRandom(ofGetWindowWidth()), 0, ofRandom(ofGetWindowWidth()), ofGetWindowHeight() );
                    ofLine(0, ofRandom(ofGetWindowHeight()), ofGetWindowWidth(), ofRandom(ofGetWindowHeight()) );
                }
            }
            
            //circulos blancos
            for(int i=0;i < listaCirculos.size(); i++){
                listaCirculos[i].draw();
            }
            
            
            ofSetColor(255, 0, 0);
            for(int i=0; i<manchasSoltadas.size(); i++){
                manchasImagen[int(manchasSoltadas[i][2])].draw( manchasSoltadas[i][0]*800-manchasImagen[int(manchasSoltadas[i][2])].getWidth()*0.5, manchasSoltadas[i][1]*600-manchasImagen[int(manchasSoltadas[i][2])].getHeight()*0.5);
                
                
            }
            fboPoliContenido.end();
            
            fboPoliContenido.readToPixels(pixelsPoliContenido);
            
            
            imagePoliContenido.setFromPixels(pixelsPoliContenido);
            imagePoliContenido.update();
            
            fboPoli.begin();
            ofClear(0, 0, 0, 0);
            
            
            shaderPoli.begin();
            shaderPoli.setUniformTexture("maskTex", fboPoliMask.getTextureReference(), 1 );
            imagePoliContenido.draw(0,0);
            
            shaderPoli.end();
            fboPoli.end();
            
            
            ofSetColor(255,255);

            
            fboPoli.draw(0, 0);
        } else if(parteHip){
            //se usan las mismas variables, aunque se llamen Poli
            
//             _  _  __  ____  _  _   __  ____    ____  ____   __   _  _
//            / )( \(  )(  _ \/ )( \ /  \(  _ \  (    \(  _ \ / _\ / )( \
//            ) __ ( )(  ) __/) __ ((  O )) __/   ) D ( )   //    \\ /\ /
//            \_)(_/(__)(__)  \_)(_/ \__/(__)    (____/(__\_)\_/\_/(_/\_)
//            
            


//                hiphopFotos[0].draw(0,0,ofGetWindowWidth(), ofGetWindowHeight());
           
                fboPoliContenido.begin();
                //ofClear(0, 0, 0, 0);
                ofSetColor(255, 0, 0);
                kinectColorImg.draw(0, 0,ofGetWindowWidth(), ofGetWindowHeight());
                fboPoliContenido.end();
                
                fboPoliContenido.readToPixels(pixelsPoliContenido);
                
                
                imagePoliContenido.setFromPixels(pixelsPoliContenido);
                imagePoliContenido.update();
                
                fboPoli.begin();
                ofClear(0, 0, 0, 0);
                
                
                shaderPoli.begin();
                shaderPoli.setUniformTexture("maskTex", fboPoliMask.getTextureReference(), 1 );
                
                for(float i=1; i>=0; i-=0.1){
                    kinect.draw(0-(ofGetWindowWidth()*i), 0-(ofGetWindowHeight()*i),ofGetWindowWidth()+(ofGetWindowWidth()*i*2), ofGetWindowHeight()+(ofGetWindowHeight()*i*2));
                }
                //kinectImagenActualColor.draw(0, 0,ofGetWindowWidth(), ofGetWindowHeight());
                shaderPoli.end();
                fboPoli.end();
                std::cout << "Dibujando hip\n";
                ofSetColor(255,255);
                
                fboPoli.draw(0, 0);
            

        }

        

        //ofDisableAlphaBlending();
        

        
    }
//    kinect.draw(0, 0, 800, 600);
//    kinect.getTextureReference();
    if(bHide){
        gui.draw();
    }
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	if( key == 'h' ){
		bHide = !bHide;
	}
	if(key == 's') {
		gui.saveToFile("settings.xml");
	}
	if(key == 'l') {
		gui.loadFromFile("settings.xml");
	}
    if(key == 'k'){
        kinect.setCameraTiltAngle(kinectAngle);
    }
    if(key == 'f'){
        if(estaFullScreen){
            estaFullScreen=false;
            ofSetFullscreen(false);
        } else {
            estaFullScreen=true;
            ofSetFullscreen(true);
        }
        
    }
    if(key == 'v'){
        camara.videoSettings();
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 
    
}

//void testApp::keyPressedEvent(ofKeyEventArgs &args)
//{
//cout << "MOUSE WAS MOVED" << endl;
//}
