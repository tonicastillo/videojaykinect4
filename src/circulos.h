#ifndef circulos_h
#define circulos_h

#include "ofMain.h"

class Circulos {
public:
    // Constructor
    Circulos(int puntox, int puntoY);
    
    // Methods
    void draw();
    
    // Properties
    int posx;
    int posy;
    int radio;
    
};
#endif
