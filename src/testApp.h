#pragma once


#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxGui.h"
#include "ofxOpenCv.h"
#include "ofxKinect.h"
#include "ofxNetwork.h"
#include "circulos.h"

#define NVIDEOS 4
#define NSONIDOS 20
#define REDUCCIONAUDIO 0.5


#define NUM_MSG_STRINGS 50


class testApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed  (int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    

    
private:
    ofxOscReceiver	oscReceiver;
    ofxOscSender    oscSender;
    int				current_msg_string;
    std::string		msg_strings[NUM_MSG_STRINGS];
    float			timers[NUM_MSG_STRINGS];
    
    
    
    
    //GUI
    bool bHide;
    
	ofxIntSlider musicaFiles;
	ofxIntSlider videoFiles;
	ofxIntSlider cortoFiles;
	ofxIntSlider manchasFiles;
    ofxIntSlider camaraId;
	ofxIntSlider kinectAngle;
    ofxIntSlider kinectNearThreshold;
	ofxIntSlider kinectFarThreshold;
    
    ofxToggle oscActivo;
	ofxLabel status;
    
	ofxPanel gui;
    
    ofxIntSlider kinectPosX;
    ofxIntSlider kinectPosY;
	ofxIntSlider kinectAncho;
	ofxIntSlider poliLineas;
	
    ofxFloatSlider volumenFondoConcurso;
    ofxFloatSlider volumenVideoIntro;
    ofxFloatSlider volumenVideoKaraoke;
    
    ofxIntSlider ipIpad1;
    ofxIntSlider ipIpad2;
    
    ofxToggle kinectTest;
	
    
    //VIDEO Y AUDIO
    ofVideoPlayer videos[NVIDEOS];
    //bool videosactivo[NVIDEOS];
    //int videosAlpha[NVIDEOS];
    //float videosposicion[NVIDEOS];
    bool bVideoLargo1;
    bool bVideoLargo2;
    float fVideoLargoPosicion1;
    float fVideoLargoPosicion2;
    
    
    
    ofVideoPlayer cortos[20];
    bool cortosactivo[20];
    
    ofSoundPlayer sonidos[2][NSONIDOS];
    bool sonidosactivo[2][NSONIDOS];
    int sonidosultimo[2];
    float sonidosposicion[2];
    
    //CONCURSO
    ofImage concursoImagenes[12];
    bool concursoImagenesActivas[12];
    
    ofVideoGrabber camara;
    ofSoundPlayer concursoSonido;
    ofVideoPlayer concursoVideo1;
    ofVideoPlayer concursoVideo2;
    
    //Kinect
    ofxKinect kinect;
    ofxCvColorImage kinectColorImg;
    ofxCvGrayscaleImage kinectGrayImage;
    ofxCvGrayscaleImage kinectGrayThreshNear; // the near thresholded image
	ofxCvGrayscaleImage kinectGrayThreshFar; // the far thresholded image
    //ofxCvContourFinder kinectContourFinder;
    ofImage kinectImagenActual;
    ofImage kinectImagenActualColor;
    
    
    //POLI
    ofFbo fboPoli;
    ofFbo   fboPoliMask;
    ofFbo fboPoliContenido;
    ofPixels pixelsPoliContenido;
    ofImage imagePoliContenido;
    ofShader shaderPoli;
    
    ofImage manchasImagen[20];
    vector<ofPoint> manchasSoltadas;
    vector<Circulos> listaCirculos;
    bool sangreActiva;
    float poliLuces;
    
    //HIPHOP
    ofImage hiphopFotos[10];
    
    
    
    ofImage srcImg; //ejemplo tutorial
    
    
    //partes
    bool parteWebcam = false;
    bool partePoli = false;
    bool parteHip = false;
    
    //Varios
    bool estaFullScreen;
    
    //Network
    ofxUDPManager udpConnection;
};
