//
//  circulos.cpp
//  kinect4
//
//  Created by Toni Castillo on 09/11/12.
//
//

#include "circulos.h"
Circulos::Circulos(int puntox, int puntoy){
    posx=puntox;
    posy=puntoy;
    radio=20;
}



void Circulos::draw(){
    if(radio<500){
        radio+=10;
        ofSetColor(255, 255, 255);
        ofNoFill();
        ofSetLineWidth(10);
        ofCircle( posx, posy, radio);
    }
}